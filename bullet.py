# -*- coding = utf-8 -*-
# @Time : 2020/11/15 17:02
# @Author : 叶伟华
# @File : bullet
# @Software : alien
from pygame.sprite import Sprite
import pygame


class Bullet(Sprite):
    """一个对飞船发射的子弹进行管理的类"""

    def __init__(self, ai_settings, screen, ship):
        """在飞船所处的位置创建一个子弹对象"""
        super().__init__()
        self.screen = screen

        # 在（0，0）处创建一个表示子弹的矩形，再设置设置正确的位置
        self.rect = pygame.Rect(0, 0,
                                ai_settings.bullet_width,
                                ai_settings.bullet_height)
        self.rect.centerx = ship.rect.centerx
        self.rect.top = ship.rect.top
        # 用小数表示子弹的位置
        self.y = float(self.rect.y)
        self.x = float(self.rect.x)

        self.color = ai_settings.bullet_color
        self.y_speed_factor = ai_settings.bullet_y_speed_factor
        self.x_speed_factor = ai_settings.bullet_x_speed_factor

    def update(self):
        """向上移动子弹"""
        # 更新表示子弹位置的小数值
        self.y -= self.y_speed_factor
        self.x -= self.x_speed_factor
        # 更新子弹的rect的位置
        self.rect.y = self.y
        self.rect.x = self.x

    def draw_bullet(self):
        """在屏幕上绘制子弹"""
        pygame.draw.rect(self.screen, self.color, self.rect)
