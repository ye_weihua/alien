# -*- coding = utf-8 -*-
# @Time : 2020/11/15 14:41
# @Author : 叶伟华
# @File : settings
# @Software : alien
class Settings:
    """ 存储《外星人入侵》的所有设置类"""

    def __init__(self):
        """初始化游戏的设置"""
        self.screen_width = 1200
        self.screen_height = 600
        self.bg_color = (230, 230, 230)
        # 飞船的设置
        self.ship_speed_factor = 0.25
        # 子弹设置
        # 子弹y轴的移动速度的设置
        self.bullet_y_speed_factor = 0.5
        # 子弹x轴的移动速度的设置
        self.bullet_x_speed_factor = 0
        self.bullet_width = 3
        self.bullet_height = 15
        self.bullet_color = (60,60,60)
        # 限制子弹数量
        self.bullet_allowed = 1000
