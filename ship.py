# -*- coding = utf-8 -*-
# @Time : 2020/11/15 15:05
# @Author : 叶伟华
# @File : ship
# @Software : alien
import pygame


class Ship:
    def __init__(self, ai_settings, screen):
        """初始化飞船，并设置其初始位置"""
        self.screen = screen
        self.ai_settings = ai_settings

        # 加载飞船图片并获取其外接矩形
        self.image = pygame.image.load('images/ship.bmp')
        # self.image = pygame.transform.scale(image, (70, 110))
        # get_rect 获取矩形的长宽中心坐标
        self.rect = self.image.get_rect()
        self.screen_rect = screen.get_rect()

        # 将每艘新飞船放在屏幕底部中央
        # centerx x轴中心
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom
        # self.rect.centery = self.screen_rect.centery

        # 在飞船的属性center中存储小数值
        self.centerX = float(self.rect.centerx)
        self.centerY = float(self.rect.centery)

        # 移动标志
        self.moving_right = False
        self.moving_left = False
        self.moving_up = False
        self.moving_down = False

    def update(self):
        """根据移动标志调整飞船的位置"""
        # 更新飞船的center值，而不是rect
        if self.moving_right and self.rect.centerx < self.screen_rect.right:
            self.centerX += self.ai_settings.ship_speed_factor
        if self.moving_left and self.rect.centerx > 0:
            self.centerX -= self.ai_settings.ship_speed_factor
        if self.moving_up and self.rect.top > 0:
            self.centerY -= self.ai_settings.ship_speed_factor
        if self.moving_down and self.rect.bottom < self.screen_rect.bottom:
            self.centerY += self.ai_settings.ship_speed_factor

        # 根据self.center 更新rect对象
        self.rect.centerx = self.centerX
        self.rect.centery = self.centerY

    def blitMe(self):
        """在指定位置绘制飞船"""
        self.screen.blit(self.image, self.rect)
