# -*- coding = utf-8 -*-
# @Time : 2020/11/15 14:27
# @Author : 叶伟华
# @File : alien_invasion
# @Software : alien
import pygame
from settings import Settings
from ship import Ship
from alien import Alien
import game_functions as gf
from pygame.sprite import Group


def run_game():
    # 初始化游戏，并创建一个屏幕对象
    pygame.init()
    ai_settings = Settings()
    screen = pygame.display.set_mode(
        (ai_settings.screen_width, ai_settings.screen_height))
    pygame.display.set_caption("外星人入侵")

    # 创建一艘飞船
    ship = Ship(ai_settings, screen)
    # 创建一个用于存储子弹的编组
    bullets = Group()
    # 创建外星人群
    aliens = Group()
    # 创建外星人群
    gf.create_fleet(ai_settings, screen, ship, aliens)
    # 创建一个外星人
    # alien = Alien(ai_settings, screen)

    # 开始游戏的主循环
    while True:
        # 监听按键事件
        gf.check_events(ai_settings, screen, ship, bullets)
        # 飞船移动
        ship.update()
        # 更新子弹位置
        gf.update_bullets(bullets)
        # 重绘屏幕
        gf.update_screen(ai_settings, screen, ship, aliens, bullets)


run_game()
